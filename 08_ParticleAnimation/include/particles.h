#ifndef PARTICLES_H
#define PARTICLES_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>

struct Particle {
	glm::vec3   p0;
	glm::vec3	position;
	glm::vec3	velocity;
	glm::vec3	acceleration;
	glm::vec3	force;
	float		mass;
};

class Particles 
{
public:
	// Material Attributes
	vector<Particle> particles;

	Particles(unsigned int numParticles) {
		for (unsigned int i = 0; i < numParticles; i++) {
			Particle P;
			P.p0.x = rand() % 25-12; // posición random de -5 a 10
			P.p0.y = rand() % 8;    // posición random de 0 a 5
			P.p0.z = rand() % 25-12; // posición random de -5 a 10

			P.position = P.p0;
			P.velocity = glm::vec3(0.0f, 0.0f, 0.0f);
			P.acceleration = glm::vec3(0.0f, 0.0f, 0.0f);
			P.force = glm::vec3(0.0f, 0.0f, 0.0f);
			P.mass = 0.001f;
			
			particles.push_back(P);
		}
	}
	~Particles() {}

	void setGravity(glm::vec3 g) { gravity = g; }
	glm::vec3 getGravity() { return gravity; }

	void UpdatePhysics(float deltaTime) {
		for (int i = 0; i < (int)(particles.size()); i++) {
			Particle *P = &particles.at(i);
			if (P->position.y < 0) P->position = P->p0;  // retornamos a la posición original 
			                                          // de la partícula cuando llega al piso
			P->force = P->mass * gravity; 
			P->acceleration = P->force / P->mass;
			P->velocity += P->acceleration * deltaTime;
			P->position += P->velocity * deltaTime;
		}
	}

private:

	glm::vec3 gravity = glm::vec3(0.0f, -0.1f, 0.0f);

};

#endif
